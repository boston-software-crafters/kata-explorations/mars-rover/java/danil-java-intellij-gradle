# Mars Rover Kata Starter Project

This project is an exploration developed for the monthly [Boston Software Crafters](https://boston-software-crafters.github.io/website/) meet-up event using Java with IntelliJ and Gradle.

The most relevant and interesting information is the [Mars Rover Kata overview](MarsRover.md) where you will learn all about the rules and goals for the kata.

Next in importance is a short discussion of [using TDD on this kata](TDD.md).

Note [the basic requirements](Prerequisites.md) for working on this kata with Java, IntelliJ and Gradle.

The rest of this document is for notes to be added during the event by the mobbers.

Team <Name> Members
*  tbd (facilitator)
*  tbd
*  tbd
*  tbd
